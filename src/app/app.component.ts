import { Component } from '@angular/core';
import { QueryResult, ReportEntry } from './types';
import { groupResponseByStation } from './utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  stationEntries: Map<string, ReportEntry[]>;

  onResultChange(newData: QueryResult[]) {
    console.log(newData);
    this.stationEntries = groupResponseByStation(newData);
  }

  onSubmitClick() {
    this.stationEntries = null;
  }
}
