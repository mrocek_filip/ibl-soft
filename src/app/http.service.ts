import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OPMETRequest, OPMETResponse, createOPMETRequest } from './types';
import { Observable, of } from 'rxjs';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private pilotBriefingUrl = 'https://ogcie.iblsoft.com/ria/opmetquery';
  private static counter = 0;

  constructor(private httpClient: HttpClient) { }

  public getPilotBriefing(airports: string[], countries: string[], types: string[]): Observable<OPMETResponse> {
    const request = this.buildRequest(airports, countries, types);
    // to prevent OPTIONS preflight, use this content type
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
      })
    };

    return this.httpClient.post<OPMETResponse>(this.pilotBriefingUrl, request, httpOptions).pipe(
      timeout(30000)
    );
  }

  private buildRequest(airports: string[], countries: string[], types: string[]): OPMETRequest {
    return {
      id: (++HttpService.counter).toString(),
      method: 'query',
      params: [
        {
          id: null,
          countries: countries,
          stations: airports,
          reportTypes: types
        }
      ]
    };
  }

  public getMockBriefingData(): any {
    return of(
      {
        "error": null,
        "id": "query01",
        "result": [
          {
            "placeId": "icao:EGLL",
            "queryType": "METAR",
            "receptionTime": "2016-06-15T10:54:21.218Z",
            "refs": [
              "briefing01"
            ],
            "reportTime": "2016-06-15T10:50:00Z",
            "reportType": "MSG_METAR",
            "revision": "COR",
            "stationId": "EGLL",
            "text": "EGLL 151050Z AUTO 18008KT 130V250 9999 FEW033/// //////CB 19/11 Q0996 TEMPO SHRA=",
            "textHTML": "EGLL 151050Z AUTO 18008KT 130V250 <font color=\"blue\">9999</font> FEW033/// //////CB 19/11 Q0996 TEMPO SHRA="
          },
          {
            "placeId": "icao:LKPR",
            "queryType": "METAR",
            "receptionTime": "2016-06-15T11:03:10.171Z",
            "refs": [
              "briefing01"
            ],
            "reportTime": "2016-06-15T11:00:00Z",
            "reportType": "MSG_METAR",
            "stationId": "LKPR",
            "text": "LKPR 151100Z 23007KT 9999 BKN017 16/12 Q1002 NOSIG=",
            "textHTML": "LKPR 151100Z 23007KT 9999 <font color=\"white\">BKN017</font> 16/12 Q1002 NOSIG="
          }
        ]
      }
    );
  }
}
