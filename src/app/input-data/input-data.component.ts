import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, ValidatorFn, FormBuilder, ValidationErrors } from '@angular/forms';
import { HttpService } from '../http.service';
import { OPMETResponse, QueryResult, ReportTypes } from '../types';

@Component({
  selector: 'app-input-data',
  templateUrl: './input-data.component.html',
  styleUrls: ['./input-data.component.css']
})
export class InputDataComponent implements OnInit {
  @Output() onDataChange: EventEmitter<QueryResult[]> = new EventEmitter<QueryResult[]>();
  @Output() submitClick: EventEmitter<null> = new EventEmitter<null>();

  isLoadingData = false;
  showError = false;
  briefingForm = this.formBuilder.group({
    metar: [false],
    sigmet: [false],
    taf: [false],
    airports: ['', airportsValidator],
    countries: ['', countriesValidator]
  }, {
      validator: [airportCountryValidator, typesValidator]
    });

  get controls() { return this.briefingForm.controls; }
  get isBtnDisabled() { return this.briefingForm.errors || this.isLoadingData || !this.briefingForm.valid}

  constructor(private formBuilder: FormBuilder, private httpService: HttpService) { }

  ngOnInit() {
  }

  onSubmitClick() {
    if (this.briefingForm.invalid) {
      return;
    }
    this.submitClick.emit();
    this.isLoadingData = true;
    this.showError = false;

    let airportsCodes = this.controls.airports.value === '' ? null : this.controls.airports.value.trim().split(' ');
    let countryCodes = this.controls.countries.value === '' ? null : this.controls.countries.value.trim().split(' ');
    this.httpService.getPilotBriefing(airportsCodes, countryCodes, this.getMessageTypes()).subscribe(
      (response: OPMETResponse) => {
        if (response.error) {
          this.onError(response.error);
        }
        this.isLoadingData = false;
        this.onDataChange.emit(response.result);
      },
      (err) => {
          this.onError(err);
      }
    );
  }

  onShowMockDataClick() {
    this.httpService.getMockBriefingData().subscribe(
      (data) => {
        this.isLoadingData = false;
        this.onDataChange.emit(data.result);
      }
    )
  }

  private onError(err) {
    console.log(err);
    this.isLoadingData = false;
    this.showError = true;
    this.onDataChange.emit(null);
  }

  private getMessageTypes(): string[] {
    let types = [];
    if (this.controls.metar.value === true) types.push(ReportTypes.METAR);
    if (this.controls.sigmet.value === true) types.push(ReportTypes.SIGMET);
    if (this.controls.taf.value === true) types.push(ReportTypes.TAF);
    return types;
  }
}

export const airportCountryValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const airports = control.get('airports');
  const countries = control.get('countries');

  return airports && countries && airports.value == '' && countries.value == '' ? { 'airportCountryMissing': true } : null;
};

export const typesValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const metar = control.get('metar');
  const sigmet = control.get('sigmet');
  const taf = control.get('taf');

  return metar.value == '' && sigmet.value == '' && taf.value == '' ? { 'typeMissing': true } : null;
};

export const airportsValidator: ValidatorFn = (control: FormControl): ValidationErrors | null => {
  const airportRegex = /^([A-Z]{4}(\s|$))+$/;
  // if control.value is empty, different validator is taking care, so return VALID
  return airportRegex.test(control.value) || control.value === "" ? null : { 'airportsInvalid': true };
};

export const countriesValidator: ValidatorFn = (control: FormControl): ValidationErrors | null => {
  const countriesRegex = /^([A-Z]{2}(\s|$))+$/;
  // if control.value is empty, different validator is taking care, so return VALID
  return countriesRegex.test(control.value) || control.value === "" ? null : { 'countriesInvalid': true };
};