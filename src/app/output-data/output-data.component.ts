import { Component,Input } from '@angular/core';
import { ReportEntry } from '../types';

@Component({
  selector: 'app-output-data',
  templateUrl: './output-data.component.html',
  styleUrls: ['./output-data.component.css']
})
export class OutputDataComponent {
  @Input() stationEntries: Map<string, ReportEntry[]>;
  constructor() { }

  ngOnChanges(changes) {
    console.log(changes);
  }
}
