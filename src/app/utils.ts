import { QueryResult, ReportEntry } from "./types";

export function groupResponseByStation(responseData: QueryResult[]): Map<string, ReportEntry[]> {
    const stationMap: Map<string, ReportEntry[]> = new Map();
    if (responseData == null || responseData.length === 0) {
        return stationMap;
    }

    responseData.forEach((qResult: QueryResult) => {
        let stationEntry: ReportEntry = {
            reportType: qResult.queryType,
            reportTime: qResult.reportTime,
            reportBody: qResult.text
        }
        if (stationMap.get(qResult.stationId) == null) {
            stationMap.set(qResult.stationId, []);
        }
        stationMap.get(qResult.stationId).push(stationEntry);
    });

    return stationMap;
}