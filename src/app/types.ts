export interface OPMETRequest {
    id: string;
    method: string;
    params: [
        {
            id: string;
            reportTypes: string[];
            stations: string[];
            countries: string[];
        }
    ]
}

export function createOPMETRequest(queryId = null, briefingId = null): OPMETRequest {
    return {
        id: queryId,
        method: "query",
        params: [
            {
                id: briefingId,
                reportTypes: [],
                stations: [],
                countries: [],
            }
        ]
    }
}

export interface OPMETResponse {
    error: any;
    id: string;
    result: QueryResult[]
}

export interface QueryResult {
    reportTime: Date;
    queryType: ReportTypes;
    reportType: string;
    stationId: string;
    text: string;
    textHTML: string;
}

// export interface StationEntry {
//     stationId: string;
//     reports: ReportEntry[]
// }

export interface ReportEntry {
    reportType: ReportTypes;
    reportTime: Date;
    reportBody: string;
}

export enum ReportTypes {
    METAR = 'METAR',
    SIGMET = 'SIGMET',
    TAF = 'TAF'
}
