import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppComponent } from './app.component';
import { InputDataComponent } from './input-data/input-data.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './http.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { OutputDataComponent } from './output-data/output-data.component';

import { registerLocaleData } from '@angular/common';
import localeSk from '@angular/common/locales/sk';
import { ColorizeReportBodyPipe } from './colorize-report-body.pipe';
registerLocaleData(localeSk);

@NgModule({
  declarations: [
    AppComponent,
    InputDataComponent,
    OutputDataComponent,
    ColorizeReportBodyPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ],
  providers: [
    HttpService,
    { provide: LOCALE_ID, useValue: "sk-SK" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
