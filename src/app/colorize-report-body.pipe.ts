import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'colorizeReportBody'
})
export class ColorizeReportBodyPipe implements PipeTransform {
  transform(value: string, args?: any): any {
    const regex = /(BKN|FEW|SCT)\d{3}/g;
    let occurences: string[] = value.match(regex);
    if (occurences == null || occurences.length === 0) {
      return value;
    }

    let newStr = value; 
    occurences.forEach((occurence: string) => {
      let num = Number.parseInt(occurence.substr(3));
      const fontColor = num <= 30 ? 'blue' : 'red';
      newStr = newStr.replace(occurence, `<span class="${fontColor}">${occurence}</span>`);
    });

    return newStr;
  }

}
